package com.lalo.retrofit_fragments

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.lalo.retrofit_fragments.ConfiguracionInicial.JsonPlaceHolder
import com.lalo.retrofit_fragments.Models.Publicacion
import com.lalo.retrofit_fragments.Models.photos
import com.lalo.retrofit_fragments.databinding.ActivityMainBinding
import retrofit2.Call

class MainActivity : AppCompatActivity() ,JsonPlaceHolder{
    lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, Recuperafotos() )
        transaction.addToBackStack(null)
        transaction.commit()

    }

    override fun recuperapublicaciones(): Call<ArrayList<Publicacion>> {
        TODO("Not yet implemented")
    }

    override fun recuperapics(): Call<ArrayList<photos>> {
        TODO("Not yet implemented")
    }

}